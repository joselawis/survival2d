﻿using System.Collections.Generic;
using Enums;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public class PlayerInfoUI : MonoBehaviour {
	private const string HEALTH_LABEL = "Vida: ";
	private const string WEAPON_LABEL = "Arma: ";
	private const string CLIP_AMMO_LABEL = "Munición: ";
	private const string AMMO_LABEL = "Municiones: \n";

	[Header ("Text Fields")]
	[ReadOnly] public PlayerManager Manager;
	[SerializeField] private Text _healthText;
	[SerializeField] private Text _mainWeaponName;
	[SerializeField] private Text _mainWeaponAmmo;
	[SerializeField] private Image _mainWeaponSprite;
	[SerializeField] private Text _secondWeaponName;
	[SerializeField] private Text _secondWeaponAmmo;
	[SerializeField] private Image _secondWeaponSprite;
	[SerializeField] private Text _ammoList;

	[Header ("Info")]
	[SerializeField][ReadOnly] private float _currHealth;
	[SerializeField][ReadOnly] private float _maxHealth = 100f;
	[SerializeField][ReadOnly] private Weapon _mainWeapon;
	[SerializeField][ReadOnly] private Weapon _secondWeapon;
	[SerializeField][ReadOnly] private int _clipAmmo;
	[SerializeField][ReadOnly] private int _clipCapacity = 10;
	[SerializeField][ReadOnly] private int _clipAmmoSecond;
	[SerializeField][ReadOnly] private int _clipCapacitySecond = 10;
	[SerializeField][ReadOnly] private PlayerInventory _inventory;
	[SerializeField][ReadOnly] private Health _health;

	private void Start() {
		_inventory = Manager.Player.Inventory;
		_health = Manager.Player.GetComponent<Health>();
	}

	private void LateUpdate () {
		_currHealth = _health.HealthPoints;
		
		// Main weapon data
		_mainWeapon = _inventory.GetMainWeapon();
		_clipAmmo = (_mainWeapon) ? _mainWeapon.ClipAmmo : 0;
		_clipCapacity = (_mainWeapon) ? _mainWeapon.ClipCapacity : 0;

		// Secondary weapon data
		_secondWeapon = _inventory.GetSecondWeapon();
		_clipAmmoSecond = (_secondWeapon) ? _secondWeapon.ClipAmmo : 0;
		_clipCapacitySecond = (_secondWeapon) ? _secondWeapon.ClipCapacity : 0;
		
		UpdateLabels ();
	}

	private void UpdateLabels () {
		// Health canvas
		_healthText.text = HEALTH_LABEL + _currHealth + "/" + _maxHealth;

		// Main weapon canvas
		_mainWeaponName.text = WEAPON_LABEL + ((_mainWeapon) ? _mainWeapon.name : "NO WEAPON");
		_mainWeaponAmmo.text = CLIP_AMMO_LABEL + _clipAmmo + "/" + _clipCapacity;
		_mainWeaponSprite.sprite = (_mainWeapon) ? _mainWeapon.GetComponentInChildren<SpriteRenderer>().sprite : null;

		// Secondary weapon canvas
		_secondWeaponName.text = WEAPON_LABEL + ((_secondWeapon) ? _secondWeapon.name : "NO WEAPON");
		_secondWeaponAmmo.text = CLIP_AMMO_LABEL + _clipAmmoSecond + "/" + _clipCapacitySecond;
		_secondWeaponSprite.sprite = (_secondWeapon) ? _secondWeapon.GetComponentInChildren<SpriteRenderer>().sprite : null;

		// Ammo list canvas
		_ammoList.text = AMMO_LABEL;
		foreach(KeyValuePair<BulletTypeEnum, int> entry in _inventory.AmmoStore){
			_ammoList.text += entry.Key + ": " + entry.Value + "\n";
		}
	}
}