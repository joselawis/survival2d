using UnityEngine;

public class Shield : MonoBehaviour {
    private const float MAX_SP = 100f;
    
    [SerializeField] private float _shieldPoints = MAX_SP;
    
    /// <summary>
    /// Aplica daño al Shield
    /// </summary>
    /// <param name="damagePoints"></param>
    /// <returns>El daño entrante menos el daño absorvido</returns>
    public float EnterDamage (float damagePoints) {
        float spRestant = ShieldPoints - damagePoints;
        ShieldPoints = spRestant;
        return damagePoints - spRestant;
    }

    public void RestoreSP (float percentage) {
        float spRestored = MAX_SP * (percentage / 100);
        ShieldPoints += spRestored;
    }

    private void Die () {
        gameObject.SetActive (false);
    }

    public float ShieldPoints {
        get { return _shieldPoints; }
        private set {
            _shieldPoints = value;
            if (_shieldPoints <= 0) {
                _shieldPoints = 0;
                Die ();
            } else {
                gameObject.SetActive (true);
                if (_shieldPoints > MAX_SP) _shieldPoints = MAX_SP;
            }
        }
    }
}