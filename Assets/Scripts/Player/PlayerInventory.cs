using System.Collections.Generic;
using Enums;
using LawisNamespace;
using UnityEngine;

[RequireComponent (typeof (PlayerController))]
public class PlayerInventory : MonoBehaviour {
    private const int MAX_WEAPONS = 2;
    private const int MAX_AMMO_PER_CALIBRE = 100;
    private PlayerController _controller;
    [SerializeField][ReadOnly] private Weapon[] _weapons = new Weapon[MAX_WEAPONS];
    [SerializeField][ReadOnly] private Dictionary<BulletTypeEnum, int> _ammoStore;

    private void Awake () {
        _controller = GetComponent<PlayerController> ();
        _ammoStore = new Dictionary<BulletTypeEnum, int> ();
    }

    public void HandleShoot () {
        Weapon w = GetMainWeapon ();
        if (w && !w.IsClipEmpty ()) {
            if (w.ShootingMode == ShootingModeEnum.AUTO && Input.GetMouseButton (0)) {
                w.Shoot (_controller.State.IsCurrentState (PlayerState.AIMING));
            } else if (w.ShootingMode == ShootingModeEnum.SEMI && Input.GetMouseButtonDown (0)) {
                w.Shoot (_controller.State.IsCurrentState (PlayerState.AIMING));
            }
        }
        Debugger.DrawLineToMouse (transform.position, _controller.Manager.Camera, Color.green, 0.1f);
        //Debugger.PointerMessage (". " + Utils.GetMouseWorldPosition2D (_camera));
    }

    public void HandleReloading () {
        Weapon w = GetMainWeapon ();
        if (w && w.IsClipEmpty () && !_controller.State.IsCurrentState (PlayerState.RELOADING)) {
            _controller.Manager.ReloadUI.AlertReload (HasAmmoOfType (w.BulletType) ? "Reload [R]" : "No Ammo");
        } else {
            _controller.Manager.ReloadUI.AlertReload ("");
        }
        if (Input.GetKeyDown (KeyCode.R) && w && !w.IsClipFull ()) {
            if (HasAmmoOfType (w.BulletType)) {
                _controller.State.ReturnToIdle ();
                ToogleAiming (false);
                _controller.State.ChangeStateTo (PlayerState.RELOADING);
                _controller.Manager.ReloadUI.ReloadAmmo (w.ReloadTime);
            }
        }
    }

    public void FinalizeReloading () {
        Weapon w = GetMainWeapon ();
        int bulletsReloaded = w.Reload (GetAmmoOfType (w.BulletType));
        TakeAmmo (w.BulletType, bulletsReloaded);
        _controller.State.ReturnFromState (PlayerState.RELOADING);
    }
    public void CatchItems () {
        if (_controller.Collision.HasItemNear () && Input.GetKeyDown (KeyCode.E)) {
            Weapon w = _controller.Collision.GetItemNear ().GetComponent<Weapon> ();
            if (w) {
                AddWeapon (w);
                _controller.Collision.DeleteNearTo ();
            }
        }
    }

    public void CatchAmmoBox (AmmoBox ammoBox) {
        if (HasAmmoOfType (ammoBox.BulletType)) {
            if (_ammoStore[ammoBox.BulletType] >= MAX_AMMO_PER_CALIBRE) {
                return;
            }
            _ammoStore[ammoBox.BulletType] += ammoBox.Ammo;
        } else {
            _ammoStore.Add (ammoBox.BulletType, ammoBox.Ammo);
        }
        if (_ammoStore[ammoBox.BulletType] > MAX_AMMO_PER_CALIBRE) {
            ammoBox.Ammo = _ammoStore[ammoBox.BulletType] - MAX_AMMO_PER_CALIBRE;
            _ammoStore[ammoBox.BulletType] = MAX_AMMO_PER_CALIBRE;
        } else {
            ammoBox.Ammo = 0;
        }
    }

    private void AddWeapon (Weapon weapon) {
        GameObject newWeapon = weapon.gameObject;
        // Comprobamos si hay slot para el arma;
        for (int i = 0; i < MAX_WEAPONS; i++) {
            if (_weapons[i] == null) {
                // Añadimos el arma como hijo
                newWeapon.transform.SetParent (transform, false);
                newWeapon.transform.localPosition = Vector3.zero;
                newWeapon.transform.localRotation = Quaternion.identity;
                _weapons[i] = newWeapon.GetComponent<Weapon> ();
                _weapons[i].SwitchItemWeapon ();
                // Cambiamos el estado del arma de Item a Weapon
                newWeapon.SetActive (i == 0);
                return;
            }
        }

        // Si no encuentra hueco se sustituye el arma principal
        GameObject oldWeapon = _weapons[0].gameObject;

        // Primero asignamos como padre de la nueva arma al player y la caja al de la antigua
        oldWeapon.transform.SetParent (null, true);
        newWeapon.transform.SetParent (transform, false);
        newWeapon.transform.localPosition = Vector3.zero;
        newWeapon.transform.localRotation = Quaternion.identity;

        // Asignamos el arma nueva como arma principal
        _weapons[0] = newWeapon.GetComponent<Weapon> ();
        _weapons[0].SwitchItemWeapon ();

        oldWeapon.GetComponent<Weapon> ().SwitchItemWeapon ();
    }

    public void TakeAmmo (BulletTypeEnum type, int bullets) {
        _ammoStore[type] -= bullets;
    }

    public void SwitchWeapon () {
        if (Input.GetMouseButtonDown (2)) {
            Weapon aux = _weapons[0];
            _weapons[0] = _weapons[1];
            _weapons[1] = aux;

            if (_weapons[0]) _weapons[0].gameObject.SetActive (true);
            if (_weapons[1]) _weapons[1].gameObject.SetActive (false);
        }
    }

    public bool HasAmmoOfType (BulletTypeEnum bulletType) {
        int value;
        return _ammoStore.TryGetValue (bulletType, out value);
    }

    public int GetAmmoOfType (BulletTypeEnum bulletType) {
        int value;
        _ammoStore.TryGetValue (bulletType, out value);
        return value;
    }

    public int AddAmmo (BulletTypeEnum bulletType, int quantity) {
        if (HasAmmoOfType (bulletType)) {
            if (_ammoStore[bulletType] >= MAX_AMMO_PER_CALIBRE) {
                return quantity;
            }
            _ammoStore[bulletType] += quantity;
        } else {
            _ammoStore.Add (bulletType, quantity);
        }
        // Check ammo is not over max
        if (_ammoStore[bulletType] > MAX_AMMO_PER_CALIBRE) {
            quantity = _ammoStore[bulletType] - MAX_AMMO_PER_CALIBRE;
            _ammoStore[bulletType] = MAX_AMMO_PER_CALIBRE;
        } else {
            quantity = 0;
        }
        return quantity;
    }

    public bool HasMainWeapon () {
        return _weapons[0] != null;
    }

    public Weapon GetMainWeapon () {
        return _weapons[0];
    }

    public Weapon GetSecondWeapon () {
        return _weapons[1];
    }

    public void ToogleAiming (bool value) {
        if (HasMainWeapon ()) {
            _weapons[0].Aiming = value;
        }
    }

    public Dictionary<BulletTypeEnum, int> AmmoStore {
        get { return _ammoStore; }
    }

}