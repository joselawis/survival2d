using UnityEngine;
using UnityEngine.UI;

namespace LawisNamespace {

    public static class Debugger {

        public static GameObject debugCanvas;

        public static void MethodCall (string script, string method) {
            Debug.Log (script + " calling method: " + method);
        }

        public static void DrawLog (string script, string message) {
            Debug.Log (script + " says: " + message);
        }

        public static void PointerMessage (string message) {
            if (!debugCanvas) {
                debugCanvas = GameObject.Find ("DebugCanvas");
            }
            GameObject floatingText = Utils.GetChildGameObject (debugCanvas, "FloatingText", true);
            floatingText.transform.position = Input.mousePosition;
            floatingText.GetComponent<Text> ().text = message;
            floatingText.SetActive (true);
        }

        public static void DrawLineToMouse (Vector3 fromPosition, Camera camera, Color color, float deathTime) {
            Vector3 pointerPosition = Utils.GetMouseWorldPosition2D (camera);
            DrawLineToTarget (fromPosition, pointerPosition, color, deathTime);
        }

        public static void DrawLineToTarget (Vector3 fromPosition, Vector3 targetPosition, Color color, float deathTime) {
            Debug.DrawLine (targetPosition, fromPosition, color, deathTime);
        }

        public static void DrawShootCone (Transform tf, float degrees, float length) {
            Vector3 shotVector = Utils.VectorFromAngle (tf.up, degrees);
            Vector3 shotVector2 = Utils.VectorFromAngle (tf.up, -degrees);
            DrawLineToTarget (tf.position, tf.position + shotVector * length, Color.blue, 0f);
            DrawLineToTarget (tf.position, tf.position + shotVector2 * length, Color.blue, 0f);
        }
    }
}