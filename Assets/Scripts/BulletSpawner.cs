using Enums;
using LawisNamespace;
using UnityEngine;
using Utils.Pool;

public class BulletSpawner : Spawner {
    private const float DEBUG_LINE_RANGE = 5f;
    private const float DEBUG_LINE_TIME = 5f;

    public void SpawnBullet(BulletTypeEnum bulletType, float dispersion = 0f, Collider2D whoShoot = null) {
        Bullet bullet = Spawn () as Bullet;
        if (bullet != null)
        {
            bullet.transform.position = transform.position;
            bullet.transform.rotation = CalculeDirection(dispersion);
            if (whoShoot) Physics2D.IgnoreCollision(bullet.GetComponent<Collider2D>(), whoShoot);
            bullet.Initialize(bulletType);
        }
    }

    private Quaternion CalculeDirection (float dispersion) {
        float dispersionRand = Random.Range (-dispersion, dispersion);
        Vector3 shotVector = LawisNamespace.Utils.VectorFromAngle (transform.up, dispersionRand);
        Debugger.DrawLineToTarget (transform.position, transform.position + shotVector * DEBUG_LINE_RANGE, Color.red, DEBUG_LINE_TIME);

        float angle = LawisNamespace.Utils.AngleBetweenPoints (Vector3.zero, shotVector);
        return Quaternion.Euler (new Vector3 (0f, 0f, -angle));
    }
}