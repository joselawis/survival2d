﻿using Enums;
using UnityEngine;
using Utils.Pool;

public class Bullet : PooledObject {
	private const float MAX_DISTANCE = 30f;
	private const float MAX_VELOCITY = 10;

	private Rigidbody2D _rb;
	private float _timer;
	private float _lifeTime;
	private float _actualVelocity;
	[ReadOnly][SerializeField] private float _percBulletDmg;
	[ReadOnly][SerializeField] private float _damageDone;

	[SerializeField] private float _damage = 100;
	[SerializeField] private float _damageEfficiency; // Porcentage de daño que persiste a los 100m. Valor entre 0 y 1
	[SerializeField] private float _damageMultiplier = 1f;

	[Space (10)]
	[Header ("Rebote Variables")]
	[SerializeField] private int _maxContacts;
	[SerializeField] private float _reboteMultiplier = 0.5f;

	private void Awake () {
		_rb = GetComponent<Rigidbody2D> ();
		_lifeTime = MAX_DISTANCE / MAX_VELOCITY;
	}

	private void FixedUpdate () {
		_actualVelocity = _rb.velocity.magnitude;
	}

	private void Update () {
		_timer += Time.deltaTime;
	}

	private void OnCollisionEnter2D (Collision2D other) {
		HandleDamage (other);
		HandleRebote (other);
	}

	public void Initialize (BulletTypeEnum bulletType) {
		_timer = 0;
		switch (bulletType) {
			case BulletTypeEnum.PISTOL:
				PistolBullet ();
				break;

			case BulletTypeEnum.SUBFUSIL:
				SubfusilBullet ();
				break;
		}
		_rb.AddForce (transform.up.normalized * MAX_VELOCITY * LawisNamespace.Utils.VELOCITY_TO_FORCE);
		Invoke ("Die", _lifeTime);
	}

	private void Die () {
		CancelInvoke ("Die");
		ReturnToPool ();
	}

	private void HandleDamage (Collision2D other) {
		Health health = other.collider.GetComponent<Health> ();
		if (health) {
			_percBulletDmg = 1 + ((_timer * _damageEfficiency) - _timer) / _lifeTime;
			DamageDone = _damage * _percBulletDmg * _damageMultiplier;
			health.EnterDamage (_damageDone);
		}
	}

	private void HandleRebote (Collision2D other) {
		if (_maxContacts == 0) {
			Die ();
		} else {
			foreach (ContactPoint2D contact in other.contacts) {
				Vector3 newVelocity = Vector3.Reflect (transform.up.normalized, contact.normal);
				_rb.velocity = Vector3.zero;
				transform.up = newVelocity;
				_rb.AddForce (transform.up.normalized * _actualVelocity * LawisNamespace.Utils.VELOCITY_TO_FORCE * _reboteMultiplier);
				_maxContacts--;
			}
		}
	}

	private void PistolBullet () {
		_damage = 40f;
		_damageEfficiency = 0;
		_damageMultiplier = 1;
		_maxContacts = 0;
		_reboteMultiplier = 0;
	}

	private void SubfusilBullet () {
		_damage = 20f;
		_damageEfficiency = 0;
		_damageMultiplier = 1;
		_maxContacts = 1;
		_reboteMultiplier = 0.5f;
	}

	private float DamageDone {
		set {
			_damageDone = value;
			if (_damageDone <= 0) _damageDone = 0f;
		}
	}
}