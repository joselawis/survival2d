﻿using UnityEngine;

public class PlayerManager : MonoBehaviour {

	private static PlayerManager _instance;

	[Header ("Prefabs")]
	[SerializeField] private Canvas _playerCanvasPrefab;
	[SerializeField] private Canvas _playerWorldCanvasPrefab;
	[SerializeField] private PlayerCamera _playerCameraPrefab;
	[SerializeField] private PlayerController _playerControllerPrefab;

	[Header ("Dependencias del Player")]
	[SerializeField][ReadOnly] private Canvas _playerCanvas;
	[SerializeField][ReadOnly] private ReloadUI _reloadUI;
	[SerializeField][ReadOnly] private PlayerInfoUI _playerInfoUI;
	[SerializeField][ReadOnly] private Canvas _playerWorldCanvas;
	[SerializeField][ReadOnly] private CatchItemUI _catchItemUI;
	[SerializeField][ReadOnly] private PlayerCamera _playerCamera;
	[SerializeField][ReadOnly] private PlayerController _playerController;

	private void Awake () {
		if (!_instance) {
			_instance = this;
		} else if (_instance != this) {
			Destroy (gameObject);
		}
		gameObject.name = "PlayerManager";
		InstancePlayerCanvas ();
		InstancePlayerController ();
		InstancePlayerCamera ();
		//DontDestroyOnLoad (gameObject);
	}

	public static PlayerManager Instance {
		get { return _instance; }
	}

	private void InstancePlayerCanvas () {
		_playerCanvas = Instantiate (_playerCanvasPrefab, transform);
		_playerCanvas.name = _playerCanvasPrefab.name;
		_reloadUI = _playerCanvas.GetComponentInChildren<ReloadUI> ();
		_reloadUI.Manager = this;
		_playerInfoUI = _playerCanvas.GetComponentInChildren<PlayerInfoUI>();
		_playerInfoUI.Manager = this;

		_playerWorldCanvas = Instantiate (_playerWorldCanvasPrefab, transform);
		_playerWorldCanvas.name = _playerWorldCanvasPrefab.name;
		_catchItemUI = _playerWorldCanvas.GetComponentInChildren<CatchItemUI> ();
	}

	private void InstancePlayerController () {
		_playerController = Instantiate (_playerControllerPrefab, transform);
		_playerController.name = _playerControllerPrefab.name;
		_playerController.Manager = this;
	}

	private void InstancePlayerCamera () {
		_playerCamera = Instantiate (_playerCameraPrefab, transform);
		_playerCamera.name = _playerCameraPrefab.name;
		_playerCamera.Player = _playerController;
	}

	public Camera Camera {
		get { return _playerCamera.GetComponent<Camera> (); }
	}

	public ReloadUI ReloadUI {
		get { return _reloadUI; }
	}

	public CatchItemUI CatchItemUI {
		get { return _catchItemUI; }
	}

	public PlayerController Player {
		get { return _playerController; }
	}
}