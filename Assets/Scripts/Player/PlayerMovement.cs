﻿using UnityEngine;

[RequireComponent (typeof (Rigidbody2D))]
[RequireComponent (typeof (PlayerController))]
public class PlayerMovement : MonoBehaviour {
	private const float ROTATION_MAX_SPEED = 10f;
	private const float MOVEMENT_MAX_SPEED = 10f;
	private const string HORIZONTAL_AXIS = "Horizontal";
	private const string VERTICAL_AXIS = "Vertical";

	private PlayerController _controller;
	private Rigidbody2D _rb2D;

	private void Awake () {
		_controller = GetComponent<PlayerController> ();
		_rb2D = GetComponent<Rigidbody2D> ();
	}

	private void FixedUpdate () {
		LimitVelocity ();
	}

	public void HandleMovement () {
		float horizontal = Input.GetAxis (HORIZONTAL_AXIS);
		float vertical = Input.GetAxis (VERTICAL_AXIS);
		if (horizontal != 0 || vertical != 0) {
			Vector3 direction = new Vector3 (horizontal, vertical, 0.0f);
			_rb2D.AddForce (direction.normalized * MOVEMENT_MAX_SPEED);
		}
	}

	public void LimitVelocity () {
		// Limitar la velocidad
		_rb2D.velocity = new Vector2 (
			Mathf.Clamp (_rb2D.velocity.x, -MOVEMENT_MAX_SPEED, MOVEMENT_MAX_SPEED),
			Mathf.Clamp (_rb2D.velocity.y, -MOVEMENT_MAX_SPEED, MOVEMENT_MAX_SPEED));
	}

	public void HandleRotation () {
		transform.rotation = LawisNamespace.Utils.LookAtPosition2D (transform, LawisNamespace.Utils.GetMouseWorldPosition2D (_controller.Manager.Camera), ROTATION_MAX_SPEED);
	}
}