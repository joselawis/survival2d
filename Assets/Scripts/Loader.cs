﻿using UnityEngine;

public class Loader : MonoBehaviour {
    [SerializeField] private GameObject _gameManager;

    private void Awake () {
        if (!GameManager.Instance) {
            if (!_gameManager) {
                _gameManager = new GameObject ("GameManager");
                _gameManager.AddComponent<GameManager> ();
            }
            Instantiate (_gameManager);
        }
    }
}