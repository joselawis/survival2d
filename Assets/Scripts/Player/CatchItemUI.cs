using UnityEngine;
using UnityEngine.UI;

[RequireComponent (typeof (Text))]
public class CatchItemUI : MonoBehaviour {
    private const string INPUT_TEXT = " [E]";

    private Text _textComponent;

    private void Awake () {
        _textComponent = GetComponent<Text> ();
    }

    public void Initialize (GameObject target) {
        _textComponent.text = target.name + INPUT_TEXT;
        _textComponent.rectTransform.position = target.transform.position;
        _textComponent.enabled = true;
    }

    public void Disable () {
        _textComponent.enabled = false;
    }
}