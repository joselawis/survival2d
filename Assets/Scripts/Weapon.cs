﻿using Enums;
using LawisNamespace;
using UnityEngine;

public class Weapon : MonoBehaviour {
	private const float RANGE = 5f;
	private const float SHOT_AREA_LENGTH = 5f;

	private float _timer;
	private SpriteRenderer _spriteRenderer;
	private CircleCollider2D _circleCollider;
	private LineRenderer _shotArea;
	private BulletSpawner _bulletSpawner;

	[SerializeField] private bool _aiming;
	[SerializeField] private float _dispersion = 10f;
	[SerializeField] private float _dispersionAiming = 5f;
	[SerializeField] private float _shootingDelay = 0.5f;
	[SerializeField] private ShootingModeEnum _shootingMode = ShootingModeEnum.SEMI;
	[SerializeField] private BulletTypeEnum _bulletType = BulletTypeEnum.PISTOL;
	[SerializeField] private int _clipCapacity = 8;
	[SerializeField] private int _clipAmmo;
	[SerializeField] private float _reloadTime = 3;

	private void Awake () {
		_spriteRenderer = gameObject.GetComponentInChildren<SpriteRenderer> ();
		_circleCollider = gameObject.GetComponent<CircleCollider2D> ();
		_shotArea = gameObject.GetComponent<LineRenderer> ();
		_bulletSpawner = gameObject.GetComponentInChildren<BulletSpawner> ();
	}

	private void Start() {
		gameObject.name = ToString();
	}

	private void Update () {
		_timer += Time.deltaTime;
		UpdateShotArea ();
	}

	public override string ToString(){
		return _bulletType + "-" + _shootingMode;
	}

	public void Shoot (bool isAiming) {
		if (_shootingMode != ShootingModeEnum.BLOCKED && _clipAmmo > 0) {
			if (_timer >= _shootingDelay) {
				_timer = 0f;
				_bulletSpawner.SpawnBullet (_bulletType, DispersionIfAiming (), transform.parent.GetComponent<Collider2D> ());
				_clipAmmo--;
				Debugger.DrawLog (name, "Shoot!");
			}
		}
	}

	public int Reload (int ammo) {
		int bulletsReloaded = _clipCapacity - _clipAmmo;
		if (bulletsReloaded > ammo) {
			bulletsReloaded = ammo;
		}
		_clipAmmo += bulletsReloaded;
		return bulletsReloaded;
	}

	public bool IsClipFull () {
		return _clipAmmo == _clipCapacity;
	}

	public bool IsClipEmpty () {
		return _clipAmmo == 0;
	}

	public void DibujarCono () {
		Debugger.DrawShootCone (_bulletSpawner.transform, DispersionIfAiming (), RANGE);
	}

	private void UpdateShotArea () {
		if (_shotArea.enabled) {
			float ancho = 2 * (Mathf.Tan (DispersionIfAiming () * Mathf.Deg2Rad) * SHOT_AREA_LENGTH);
			_shotArea.endWidth = ancho;
		}
	}

	private float DispersionIfAiming () {
		return _aiming ? _dispersionAiming : _dispersion;
	}

	public void SwitchItemWeapon () {
		_spriteRenderer.enabled = !_spriteRenderer.enabled;
		_circleCollider.enabled = !_circleCollider.enabled;
		_shotArea.enabled = !_shotArea.enabled;
	}

	public bool Aiming {
		set { _aiming = value; }
	}

	public ShootingModeEnum ShootingMode {
		get { return _shootingMode; }
	}

	public BulletTypeEnum BulletType {
		get { return _bulletType; }
	}

	public float ReloadTime {
		get { return _reloadTime; }
	}

	public int ClipAmmo{
		get {return _clipAmmo; }
	}

	public int ClipCapacity{
		get {return _clipCapacity; }
	}
}