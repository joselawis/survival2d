using UnityEngine;

public class Health : MonoBehaviour {
    private const float MAX_HP = 100f;
    [SerializeField] private float _healthPoints = 100f;
    public float HealthPoints {
        get { return _healthPoints; }
        private set {
            _healthPoints = value;
            if (_healthPoints <= 0) transform.SendMessage ("Die");
            if (_healthPoints > MAX_HP) _healthPoints = MAX_HP;
        }
    }

    private void Awake () {
        HealthPoints = MAX_HP;
    }

    public void EnterDamage (float damagePoints) {
        HealthPoints -= damagePoints;
    }

    public void RestoreHP (float percentage) {
        float hpRestored = MAX_HP * (percentage / 100);
        HealthPoints += hpRestored;
    }
}