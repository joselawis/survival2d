using Enums;
using LawisNamespace;
using UnityEngine;

public class GameManager : MonoBehaviour {
    private static GameManager _instance;

    private void Awake () {
        if (!_instance) {
            _instance = this;
        } else if (_instance != this) {
            Destroy (gameObject);
        }
        gameObject.name = "GameManager";
        DontDestroyOnLoad (gameObject);
    }

    private void Start () {
        Debugger.MethodCall (name, "Start");
    }

    private void Update () {
        if (Input.GetKeyDown (KeyCode.Z)) {
            AmmoBox.Create (BulletTypeEnum.PISTOL, 100, new Vector3 (5, 5, 0));
        }
    }

    public static GameManager Instance {
        get { return _instance; }
    }
}