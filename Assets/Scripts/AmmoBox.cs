using Enums;
using UnityEngine;

public class AmmoBox : MonoBehaviour {
    private static AmmoBox _prefab;

    [SerializeField] private BulletTypeEnum _bulletType = BulletTypeEnum.PISTOL;
    [SerializeField] private int _ammo = 100;

    public static AmmoBox Create (BulletTypeEnum bulletType, int ammo, Vector3? position = null, Quaternion? rotation = null) {
        if (!_prefab)
        {
            GameObject go = Resources.Load ("Prefabs/AmmoBox") as GameObject;
            if (go != null) _prefab = go.GetComponent<AmmoBox>();
        }
        AmmoBox newObject = Instantiate (_prefab);

        newObject._bulletType = bulletType;
        newObject._ammo = ammo;

        newObject.transform.position = position ?? Vector3.zero;
        newObject.transform.rotation = rotation ?? Quaternion.identity;
        newObject.gameObject.name = bulletType + "-" + ammo;

        return newObject;
    }

    private void Die () {
        Destroy (gameObject);
    }

    public int Ammo {
        get { return _ammo; }
        set {
            _ammo = value;
            gameObject.name = _bulletType + "-" + _ammo;
            if (_ammo == 0) Die ();
        }
    }
    public BulletTypeEnum BulletType {
        get { return _bulletType; }
    }
}