namespace Enums
{
    public enum PlayerState {
        IDLE,
        AIMING,
        RELOADING,
        DEAD
    }
}