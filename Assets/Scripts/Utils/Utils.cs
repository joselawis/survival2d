using UnityEngine;

namespace LawisNamespace {
    public static class Utils {
        public const float VELOCITY_TO_FORCE = 50f;
        public const float FORCE_TO_VELOCITY = 0.02f;

        public static Vector3 VectorFromAngle (Vector3 direction, float angle) {
            float degrees = angle * Mathf.Deg2Rad;
            float ca = Mathf.Cos (degrees);
            float sa = Mathf.Sin (degrees);
            float rx = direction.x * ca - direction.y * sa;

            return new Vector3 (rx, direction.x * sa + direction.y * ca, direction.z);
        }

        public static GameObject GetChildGameObject (GameObject fromGameObject, string withName, bool includeInactive) {
            Transform[] ts = fromGameObject.transform.GetComponentsInChildren<Transform> (includeInactive);
            foreach (Transform t in ts)
                if (t.gameObject.name == withName) return t.gameObject;
            return null;
        }

        public static Vector3 GetMouseWorldPosition3D (Camera camera) {
            Ray ray = camera.ScreenPointToRay (Input.mousePosition);
            RaycastHit hit;

            if (Physics.Raycast (ray, out hit)) {
                return hit.point;
            }

            return Vector3.zero;
        }

        public static Vector3 GetMouseWorldPosition2D (Camera camera) {
            Vector3 mouseWorldPosition = camera.ScreenToWorldPoint (Input.mousePosition);
            mouseWorldPosition.z = 0.0f;
            return mouseWorldPosition;
        }

        public static Quaternion LookAtPosition2D (Transform transform, Vector3 targetPoint, float rotationSpeed) {
            //Quaternion targetRotation = Quaternion.LookRotation (targetPoint - transform.position, Vector3.up);
            float angle = AngleBetweenPoints (transform.position, targetPoint);
            Quaternion targetRotation = Quaternion.Euler (new Vector3 (0f, 0f, -angle));
            // Smoothly rotate towards the target point.
            return Quaternion.Slerp (transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
        }
        public static float AngleBetweenPoints (Vector3 a, Vector3 b) {
            return Mathf.Atan2 (b.x - a.x, b.y - a.y) * Mathf.Rad2Deg;
        }
        public static Quaternion LootAtMouse (Transform transform, Camera camera, float rotationSpeed) {
            // Generate a plane that intersects the transform's position with an upwards normal.
            Plane transformPlane = new Plane (Vector3.up, transform.position);

            // Generate a ray from the cursor position
            Ray ray = camera.ScreenPointToRay (Input.mousePosition);

            // Determine the point where the cursor ray intersects the plane.
            // This will be the point that the object must look towards to be looking at the mouse.
            // Raycasting to a Plane object only gives us a distance, so we'll have to take the distance,
            //   then find the point along that ray that meets that distance.  This will be the point
            //   to look at.
            float hitdist = 0.0f;
            // If the ray is parallel to the plane, Raycast will return false.
            if (transformPlane.Raycast (ray, out hitdist)) {
                // Get the point along the ray that hits the calculated distance.
                Vector3 targetPoint = ray.GetPoint (hitdist);

                // Determine the target rotation.  This is the rotation if the transform looks at the target point.
                Quaternion targetRotation = Quaternion.LookRotation (targetPoint - transform.position);

                // Smoothly rotate towards the target point.
                return Quaternion.Slerp (transform.rotation, targetRotation, rotationSpeed * Time.deltaTime);
            }

            return transform.rotation;
        }
    }
}