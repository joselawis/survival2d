using UnityEngine;

namespace Utils.Pool
{
	public abstract class Spawner : MonoBehaviour {
		[SerializeField] private PooledObject _prefab;

		protected PooledObject Spawn () {
			PooledObject spawnObject = _prefab.GetPooledInstance<PooledObject> ();
			return spawnObject;
		}
	}
}