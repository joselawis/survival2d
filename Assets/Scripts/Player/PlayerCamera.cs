﻿using UnityEngine;

public class PlayerCamera : MonoBehaviour {
    private const float MIN_SIZE = 10f;
    private const float MAX_SIZE = 30f;
    private Camera _camera;
    [ReadOnly] private PlayerController _player;
    [SerializeField] private float _cameraDistance = 1;

    private void Awake () {
        _camera = GetComponent<Camera> ();
    }

    private void Start () {
        FollowPlayer (_player);
    }

    private void Update () {
        if (Input.GetAxis ("Mouse ScrollWheel") < 0) // back
        {
            _camera.orthographicSize = Mathf.Min (_camera.orthographicSize + 1, MAX_SIZE);
        }
        if (Input.GetAxis ("Mouse ScrollWheel") > 0) // forward
        {
            _camera.orthographicSize = Mathf.Max (_camera.orthographicSize - 1, MIN_SIZE);
        }
    }

    private void LateUpdate () {
        FollowPlayer (_player);
    }

    private void FollowPlayer (PlayerController player) {
        if (player) {
            transform.position = new Vector3 (
                player.transform.position.x,
                player.transform.position.y,
                player.transform.position.z - _cameraDistance);
        }
    }

    public PlayerController Player {
        set { _player = value; }
    }
}