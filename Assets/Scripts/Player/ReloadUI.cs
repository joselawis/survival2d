﻿using System.Collections;
using UnityEngine;
using UnityEngine.UI;

public class ReloadUI : MonoBehaviour {
	[ReadOnly] public PlayerManager Manager;
	[SerializeField] private Image _progressBarBase;
	[SerializeField] private Image _progressBar;
	[SerializeField] private Text _reloadAlert;

	public void AlertReload (string alertText) {
		_reloadAlert.text = alertText;
		_reloadAlert.enabled = true;
	}

	public void ReloadAmmo (float timeToComplete) {
		_progressBarBase.enabled = true;
		_progressBar.enabled = true;
		_reloadAlert.enabled = false;
		float rate = 1 / timeToComplete;
		StartCoroutine (RadialProgress (rate));
	}

	private IEnumerator RadialProgress (float rate) {
		float i = 0;
		while (i < 1) {
			i += Time.deltaTime * rate;
			_progressBar.fillAmount = i;
			yield return 0;
		}
		_progressBar.enabled = false;
		_progressBar.fillAmount = 0;
		_progressBarBase.enabled = false;
		Debug.Log ("ACABO LA COROUTINE -> volver a Player.IDLE");
		Manager.Player.SendMessage ("FinalizeReloading");
	}
}