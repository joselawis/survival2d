﻿using System;
using Enums;
using UnityEngine;

public class PlayerController : MonoBehaviour {

    // private string fireAxis = "Fire1";
    [Header ("Manager")]
    [SerializeField][ReadOnly] private PlayerManager _manager;
    [Header ("SubComponents")]
    [SerializeField][ReadOnly] private PlayerMovement _playerMovement;
    [SerializeField][ReadOnly] private PlayerCollision _playerCollision;
    [SerializeField][ReadOnly] private PlayerInventory _playerInventory;
    [SerializeField][ReadOnly] private PlayerStateMachine _playerStateMachine;

    private void Awake () {
        _playerMovement = gameObject.AddComponent<PlayerMovement> ();
        _playerCollision = gameObject.AddComponent<PlayerCollision> ();
        _playerInventory = gameObject.AddComponent<PlayerInventory> ();
        _playerStateMachine = gameObject.AddComponent<PlayerStateMachine>();
    }

    private void Update () {
        HandleStates ();
    }

    // Se llama cada vez que se actualiza el sistema de fisicas
    private void FixedUpdate () {
        HandleStatesFixed ();
    }

    private void Die () {
        _playerStateMachine.ReturnToIdle ();
        _playerStateMachine.ChangeStateTo (PlayerState.DEAD);
    }

    private void Reanimate () {
        _playerStateMachine.ReturnToIdle ();
    }

    private void HandleAiming () {
        if (Input.GetMouseButtonDown (1)) {
            _playerStateMachine.ChangeStateTo (PlayerState.AIMING);
            _playerInventory.ToogleAiming (true);
        }
        if (Input.GetMouseButtonUp (1)) {
            _playerStateMachine.ReturnFromState (PlayerState.AIMING);
            _playerInventory.ToogleAiming (false);
        }
        //_weapons[0].DibujarCono ();
    }

    private void FinalizeReloading () {
        if (_playerStateMachine.IsCurrentState (PlayerState.DEAD)) return;
        _playerInventory.FinalizeReloading();
    }

    private void HandleStates () {
        switch (_playerStateMachine.GetCurrentState ()) {
            case PlayerState.IDLE:
                _playerInventory.HandleReloading ();
                _playerInventory.CatchItems ();
                _playerInventory.SwitchWeapon ();
                HandleAiming ();
                break;

            case PlayerState.AIMING:
                _playerInventory.HandleReloading ();
                _playerInventory.CatchItems ();
                HandleAiming ();
                break;

            case PlayerState.RELOADING:
                break;

            case PlayerState.DEAD:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    private void HandleStatesFixed () {
        switch (_playerStateMachine.GetCurrentState ()) {
            case PlayerState.IDLE:
                _playerMovement.HandleMovement ();
                _playerMovement.HandleRotation ();
                _playerInventory.HandleShoot ();
                break;

            case PlayerState.AIMING:
                _playerMovement.HandleRotation ();
                _playerInventory.HandleShoot ();
                break;

            case PlayerState.RELOADING:
                _playerMovement.HandleMovement ();
                _playerMovement.HandleRotation ();
                break;

            case PlayerState.DEAD:
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }
    }

    public PlayerCollision Collision {
        get { return _playerCollision; }
    }

    public PlayerInventory Inventory {
        get { return _playerInventory; }
    }

    public PlayerStateMachine State {
        get{ return _playerStateMachine; }
    }

    public PlayerManager Manager {
        get { return _manager; }
        set { _manager = value; }
    }
}