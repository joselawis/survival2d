﻿using UnityEngine;

public class DieAfterTime : MonoBehaviour {
    [SerializeField] private bool _isTemporal;
    [SerializeField] private bool _mustDestroy;
    [SerializeField] private float _lifeTime;

    public float LifeTime {
        set { _lifeTime = value; }
    }

    private void Start () {
        if (_isTemporal) {
            Invoke ("Die", _lifeTime);
        }
    }

    private void Die () {
        if (_mustDestroy) {
            Destroy (gameObject);
        } else {
            gameObject.SetActive (false);
        }
    }
}