using System.Collections.Generic;
using UnityEngine;

namespace Utils.Pool
{
    public class ObjectPool : MonoBehaviour {
        private PooledObject _prefab;
        private readonly Queue<PooledObject> _poolQueue = new Queue<PooledObject> ();

        public static ObjectPool GetPool (PooledObject prefab) {
            GameObject obj;
            ObjectPool pool;
            if (Application.isEditor) {
                obj = GameObject.Find (prefab.name + " Pool");
                if (obj) {
                    pool = obj.GetComponent<ObjectPool> ();
                    if (pool) {
                        pool._prefab = prefab;
                        return pool;
                    }
                }
            }
            obj = new GameObject (prefab.name + " Pool");
            pool = obj.AddComponent<ObjectPool> ();
            pool._prefab = prefab;
            return pool;
        }

        public PooledObject GetObject () {
            PooledObject obj;
            if (_poolQueue.Count > 0) {
                obj = _poolQueue.Dequeue ();
                obj.gameObject.SetActive (true);
            } else {
                obj = Instantiate (_prefab);
                obj.transform.SetParent (transform, false);
                obj.Pool = this;
            }
            return obj;
        }

        public void AddObject (PooledObject obj) {
            obj.gameObject.SetActive (false);
            obj.transform.position = Vector3.zero;
            obj.transform.rotation = Quaternion.identity;
            _poolQueue.Enqueue (obj);
        }
    }
}