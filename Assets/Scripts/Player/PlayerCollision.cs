﻿using UnityEngine;

[RequireComponent (typeof (PlayerController))]
public class PlayerCollision : MonoBehaviour {
	private PlayerController _controller;
	[SerializeField][ReadOnly] private GameObject _nearTo;

	private void Awake () {
		_controller = GetComponent<PlayerController> ();
	}
	private void OnTriggerEnter2D (Collider2D other) {
		AmmoBox ammoBox = other.GetComponent<AmmoBox> ();
		if (ammoBox) {
			ammoBox.Ammo = _controller.Inventory.AddAmmo (ammoBox.BulletType, ammoBox.Ammo);
		}
	}

	private void OnTriggerStay2D (Collider2D other) {
		if (other.CompareTag ("Item")) {
			_nearTo = other.gameObject;
			_controller.Manager.CatchItemUI.Initialize (_nearTo);
		}
	}

	private void OnTriggerExit2D (Collider2D other) {
		if (other.CompareTag ("Item") && other.gameObject.Equals (_nearTo)) {
			_nearTo = null;
			_controller.Manager.CatchItemUI.Disable ();
		}
	}

	public GameObject GetItemNear () {
		return _nearTo;
	}

	public bool HasItemNear () {
		return _nearTo != null;
	}

	public void DeleteNearTo () {
		_nearTo = null;
	}
	/*/
		public GameObject NearToObject {
			get { return _nearTo; }
			set { _nearTo = value; }
		}*/
}