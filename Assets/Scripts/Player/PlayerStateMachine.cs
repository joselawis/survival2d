using System.Collections.Generic;
using Enums;
using UnityEngine;

public class PlayerStateMachine : MonoBehaviour {
    [SerializeField] private Stack<PlayerState> _myStates;
    private void Awake () {
        _myStates = new Stack<PlayerState> ();
        _myStates.Push (PlayerState.IDLE);
    }

    public void ChangeStateTo (PlayerState newState) {
        if (_myStates.Peek () == newState) return;
        _myStates.Push (newState);
    }

    public void ReturnFromState (PlayerState state) {
        while (state == _myStates.Peek ()) {
            _myStates.Pop ();
        }
    }

    public void ReturnToIdle () {
        _myStates.Clear ();
        _myStates.Push (PlayerState.IDLE);
    }

    public bool IsCurrentState (PlayerState state) {
        return _myStates.Peek () == state;
    }

    public PlayerState GetCurrentState(){
        return _myStates.Peek ();
    }
}