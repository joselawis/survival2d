﻿using System;
using UnityEngine;

namespace Utils.Pool
{
	public class PooledObject : MonoBehaviour {

		public ObjectPool Pool { private get; set; }

		[NonSerialized]
		private ObjectPool _poolInstanceForPrefab;

		public void ReturnToPool () {
			if (Pool) {
				Pool.AddObject (this);
			} else {
				Destroy (gameObject);
			}
		}

		public T GetPooledInstance<T> () where T : PooledObject {
			if (!_poolInstanceForPrefab) {
				_poolInstanceForPrefab = ObjectPool.GetPool (this);
			}
			return (T) _poolInstanceForPrefab.GetObject ();
		}
	}
}